# Zadania

1. Użyj zasobów w taki sposób, żeby tekst widoczny po uruchomieni głównego `Activity` aplikajic
   w pionie brzmiał `Events list (portrait)` a w poziomie `Events list (landscape)`.

2. Uruchom `EventFormActivity` w reakcji na naciśnięcie przycisku `addEventButton`. 
   Aby tego dokonać zaimplementuj i zarejestruj `OnClickListener` na przycisku,
   utwórz `Intent` z activity do uruchomienia i przekaż go systemowi za pomocą metody
   `startActivity()`.

3. Zdefiniuj layout `EventsFormActivity` tak aby zawierał on formularz pozwalający na:
 * `EditText` pozwalający na wpisanie nazwy tworzonego wydarzenia
 * widok pozwalający na wybranie liczbę dost. miejsc ( - <liczba miejsc> +)
    * dodaj ikony + i - do zasobów aplikacji
    * zadeklaruj `ImageView`, które je wyświetlą i będą zwiększały / zmniejszały liczbę
    * tekst z dostępną liczbą miejsc niech wyświetla się w `TextView` pomiędzy ikonami - i +
 * wyśrodkowany przycisk z napisem "Zapisz" - narazie bez akcji

4. Zaimplementuj poprawnie zapisywanie stanu activity tak, aby wybrana liczba miejsc
   nie zerowała się po zmianie orientacji ekranu. Zrób to nadpisując metodę `onSaveInstanceState()`
   w Activity.

5. Po naciśnięciu przycisku "Zapisz" dane (nazwa wydarzeania i liczba miejsc) powinny zostać
   przekazane do Activity z listą i wyświetlone jako `Toast.makeText().show()`.

   * wywołaj `EventsFormActivity` przez `startActivityForResult()` zamiast `startActivity()`
   * zwróć dane przez `Activity.setResult()`
   * odbierz dane w metodzie `onActivityResult()` w `MainActivity`

6. Wyświetl listę wydarzeń. Możesz do tego celu użyć wygenerowanych danych znajdujących 
   się w `FakeEvents.events`. Użyj do tego celu widoku `ListView`. Zaimplementuj własny adapter
   oparty o klasę `BaseAdapter`. Pojedynczy widok elementu listy powinien być wyrenderowany
   za pomocą `CardView` i wewnątrz widoku karty powinien wyświetlać nazwę wydarzenia.

7. Przemigruj zaimplementowaną listę do nowszego `RecyclerView`.

8. Utwórz fragment wyświetlający na ekranie tekst podany jako argument. 
   W activity dodaj przycisk, który kolejno będzie dodawał nowe fragmenty do głównego 
   kontenera każdemu nadając kolejny numer. 
   
   Przetestuj działanie stosu fragmentów (backStack), poprzez dodawanie do niego fragmentów 
   i naciskanie klawisza back.

   Zadanie zaimplementuj w kontekście `FragmentExcercisesActivity`.

9. Zaimplementuj 2 fragmenty:
   
   * `FragmentA` z widokiem pozwalającym zaznaczać i odznaczać opcje w checkboxie
   * `FragmentB` wyświetlający tekst
   
   Dodaj fragmenty do Activity tak, aby wyświetlały się równocześnie. Zaimplementuj zachowanie, 
   które po zaznaczeniu lub odznaczenie checkboxa we `FragmentA` będzie powodowało zmianę napisu 
   wyświetlanego we fragmencie B. Potraktuj `Activity` jako pośrednika.
