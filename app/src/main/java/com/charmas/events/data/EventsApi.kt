package com.charmas.events.data

import retrofit2.Call
import retrofit2.http.GET
import java.util.*

interface EventsApi {
    @GET("/events")
    fun listEvents(): Call<List<EventDto>>

    data class EventDto(val id: String, val description: EventDescriptionDto)

    data class EventDescriptionDto(val name: String, val date: Date)
}