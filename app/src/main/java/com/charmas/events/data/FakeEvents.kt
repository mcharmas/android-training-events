package com.charmas.events.data

import java.util.*

object FakeEvents {

    val events = (0..100).map {
        EventsApi.EventDto(
            it.toString(),
            EventsApi.EventDescriptionDto("Sample $it", Date())
        )
    }
}