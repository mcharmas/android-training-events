package com.charmas.events.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.charmas.events.databinding.ActivityFragmentsBinding

class FragmentExercisesActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFragmentsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFragmentsBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}