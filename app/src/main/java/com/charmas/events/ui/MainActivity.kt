package com.charmas.events.ui

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import com.charmas.events.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // TODO fire EventFormActivity
        binding.addEventButton
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menu.add("Fragments").setOnMenuItemClickListener {
            startActivity(Intent(this, FragmentExercisesActivity::class.java))
            true
        }
        return true
    }
}