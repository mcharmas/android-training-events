package com.charmas.events.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.charmas.events.databinding.ActivityFormBinding

class EventFormActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityFormBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}